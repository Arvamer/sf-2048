CCX = g++
OBJ = out/main.o out/Game.o out/Bg.o out/Settings.o
LIBS = -lsfml-graphics -lsfml-window -lsfml-system -lboost_system -lboost_filesystem
FLAGS = -g3 -c -std=c++11

all : sf2048

sf2048 : ${OBJ}
	${CCX} ${OBJ} ${LIBS} -o sf2048
	
out/main.o : src/main.cpp
	${CCX} ${FLAGS} src/main.cpp -o out/main.o
	
out/Game.o : src/Game.cpp
	${CCX} ${FLAGS} src/Game.cpp -o out/Game.o
	
out/Bg.o : src/Bg.cpp
	${CCX} ${FLAGS} src/Bg.cpp -o out/Bg.o
	
out/Settings.o : src/Settings.cpp
	${CCX} ${FLAGS} src/Settings.cpp -o out/Settings.o
	
clean : 
	rm -rf out/*.o sf2048
