//////////////////////////////////////////////////////////////////////////////////////
// The MIT License (MIT)
//
// Copyright (c) 2014 Mateusz Sieczko
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////////////

#include "Settings.hpp"

using boost::property_tree::ptree;
using namespace boost::filesystem;

Settings::Settings()
{
	recSize = 134;
	margin = 12;

	#ifdef OS_LINUX
		pa = getenv("HOME");
		pa += "/.config/sf2048";
	#elif OS_WINDOWS
		pa = getenv("APPDATA");
		pa += "/sf2048";
	#endif
	

	if(!exists(pa))
	{	
		create_directories(pa);
	}
	pa += "/sf2048.conf";

	try
	{
		ptree pt;
		read_xml(pa.string(), pt);

		animationTime = sf::seconds(pt.get("settings.animationTime", 0.15f));
		toTheEnd = pt.get("settings.mergeType", false);
		offset = pt.get("settings.offset", 1);
	} catch (std::exception &e)
	{
		toTheEnd = 0;
		offset = 1;
		animationTime = sf::seconds(0.15f);

		std::cerr << "Error: " << e.what() << ". Creating new.\n";
		save();
	}

	sepPosH = 47 + offset;
}

void Settings::save()
{
	try
	{
		ptree pt;

		pt.put("settings.animationTime", animationTime.asSeconds());
		pt.put("settings.mergeType", toTheEnd);
		pt.put("settings.offset", offset);

		write_xml(pa.string(), pt);
	} catch (std::exception &e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
	}

}

