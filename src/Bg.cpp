//////////////////////////////////////////////////////////////////////////////////////
// The MIT License (MIT)
//
// Copyright (c) 2014 Mateusz Sieczko
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////////////

#include "Bg.hpp"

Bg::Bg(const Settings *settings)
{
	//Loading font and formatting score text
	ubuntu.loadFromFile("res/fonts/Ubuntu-R.ttf");
	text_score.setFont(ubuntu);
	text_score.setCharacterSize(23);
	text_score.setColor(sf::Color::White);
	text_score.setPosition(12, 10);
	text_score.setString("Score: 0");
	score = 0;
	hightscore = 0;

	sf::Vector2f refPosition(550, 8);
	refreshT.loadFromFile("res/icons/refresh.png");

	refresh.setTexture(refreshT);
	refresh.setPosition(refPosition);

	//Creating background - line under score and 16 empty tiles (4x4)
	bg.resize(8 * 16 + 2);
	bg.setPrimitiveType(sf::Lines);

	for (unsigned int x = 0, i = 0; x < 4; x++)
	{
		for (unsigned int y = 0; y < 4; y++, i += 8)
		{
			sf::Vector2f leUpCor(settings->margin * (x + 1) + 135 * x,
					(settings->sepPosH - settings->offset) + settings->margin * (y + 1) + y * 135);

			bg[i].position = sf::Vector2f(leUpCor.x, leUpCor.y);
			bg[i + 1].position = sf::Vector2f(leUpCor.x + settings->recSize, leUpCor.y);

			bg[i + 2].position = sf::Vector2f(leUpCor.x + settings->recSize, leUpCor.y);
			bg[i + 3].position = sf::Vector2f(leUpCor.x + settings->recSize,
					leUpCor.y + settings->recSize);

			bg[i + 4].position = sf::Vector2f(leUpCor.x + settings->recSize,
					leUpCor.y + settings->recSize);
			bg[i + 5].position = sf::Vector2f(leUpCor.x, leUpCor.y + settings->recSize);

			bg[i + 6].position = sf::Vector2f(leUpCor.x, leUpCor.y + settings->recSize);
			bg[i + 7].position = sf::Vector2f(leUpCor.x, leUpCor.y);

			for (unsigned int j = 0; j < 8; j++)
				bg[i + j].color = sf::Color::Black;
		}
	}
	//Line
	bg[128].position = sf::Vector2f(0, settings->sepPosH - settings->offset);
	bg[128].color = sf::Color::Black;
	bg[129].position = sf::Vector2f(600, settings->sepPosH - settings->offset);
	bg[129].color = sf::Color::Black;
}

void Bg::draw(sf::RenderWindow *window)
{
	//Drawing background
	window->draw(bg);
	window->draw(refresh);
	window->draw(text_score);
	//window->draw(refresh);
}

void Bg::changeScore(int delta)
{
	//updating score text
	score += delta;
	if (score > hightscore)
		hightscore = score;
	text_score.setString(
			"Score: " + std::to_string(score) + "\t\t\t\t\t\t\t\t"
					+ "Hightscore: " + std::to_string(hightscore));
}

void Bg::setHightscore(int hightscore)
{
	this->hightscore = hightscore;
	changeScore(0); 						//Refreshing text
}

unsigned int Bg::getScore()
{
	return score;
}

unsigned int Bg::getHightscore()
{
	return hightscore;
}

void Bg::restart()
{
	score = 0;
	text_score.setString(
			"Score: 0\t\t\t\t\t\t\t\tHightscore: "
					+ std::to_string(hightscore));
}

