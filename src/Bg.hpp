//////////////////////////////////////////////////////////////////////////////////////
// The MIT License (MIT)
//
// Copyright (c) 2014 Mateusz Sieczko
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////////////

#ifndef Bg_H
#define Bg_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Settings.hpp"

//Class with background and score

class Bg
{
public:
	Bg(const Settings *settings);
	void draw(sf::RenderWindow *window);
	void changeScore(int delta);
	void setHightscore(int hightscore);
	unsigned int getScore();
	unsigned int getHightscore();
	void restart();

protected:
private:
	sf::Font ubuntu;
	sf::Text text_score;

	sf::Texture refreshT;
	sf::Sprite refresh;

	unsigned int score, hightscore;

	sf::VertexArray bg;
};

#endif // Bg_H
