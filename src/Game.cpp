//////////////////////////////////////////////////////////////////////////////////////
// The MIT License (MIT)
//
// Copyright (c) 2014 Mateusz Sieczko
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////////////

#include "Game.hpp"

Game::Game(Bg *bg, const Settings *settings)
{
	this->bg = bg;
	this->settings = settings;

	texture.loadFromFile("res/tileStrip.png");

	generator.seed(std::chrono::system_clock::now().time_since_epoch().count());

	tiles.resize(16 * 4);
	tiles.setPrimitiveType(sf::Quads);

	animation = false;

	#ifdef OS_LINUX
        pa = getenv("HOME");
        pa += "/.local/share/sf2048";
    #elif OS_WINDOWS
		pa = getenv("APPDATA");
        pa += "/sf2048";
    #endif

	if (!exists(pa))
	{
		create_directories(pa);
	}
	pa += "/sf2048.sav";

	std::ifstream file(pa.string());
	if (file)
	{
		for (int &i : tab)
		{
			file >> i;
		}
		int score;
		file >> score;
		bg->changeScore(score);
		file >> score;
		bg->setHightscore(score);
		file.close();

		std::copy(tab, tab + 16, tabDraw);
		update();

	}
	else
		restart();
}

void Game::restart()
{
	for (int &i : tabDraw)
	{
		i = 0;
	}

	spawnTile();
	spawnTile();
	std::copy(tabDraw, tabDraw + 16, tab);

	update();
}

void Game::save()
{
	std::ofstream file(pa.string());
	for(int &i : tabDraw)
	{
		file << i << std::endl;
	}
	file << bg->getScore() << std::endl;
	file << bg->getHightscore() << std::endl;

	file.close();
}

int Game::moveR()
{
	int deltaScore = 0;

	std::copy(tabDraw, tabDraw + 16, tab);

	for (unsigned int i = 2; i < 10; i--)
	{
		for (unsigned int j = 0; j < 4; j++)
		{
			unsigned k = j * 4 + i;
			if (!tab[k])
				continue;

			unsigned t = k;
			for (unsigned int l = i; l < 3; l++, k++)
			{
				if ((tab[k] == tab[k + 1]) && tab[k] > 0)
				{
					deltaScore += pow(2, tab[k] + 1);
					tab[k + 1]++;
					tab[k + 1] *= -1;
					tab[k] = 0;
					s[t].x += 147;
					animation = true;
				}
				else if (!tab[k + 1])
				{
					tab[k + 1] = tab[k];
					tab[k] = 0;
					s[t].x += 147;
					animation = true;
				}
			}
			s[t].z = animationFrames;
		}
	}

	return deltaScore;
}

int Game::moveL()
{
	int deltaScore = 0;

	std::copy(tabDraw, tabDraw + 16, tab);

	for (unsigned int i = 1; i < 4; i++)
	{
		for (unsigned int j = 0; j < 4; j++)
		{
			unsigned k = j * 4 + i;
			if (!tab[k])
				continue;

			unsigned t = k;
			for (unsigned int l = 0; l < i; l++, k--)
			{
				if ((tab[k] == tab[k - 1]) && tab[k] > 0)
				{
					deltaScore += pow(2, tab[k] + 1);
					tab[k - 1]++;
					tab[k - 1] *= -1;
					tab[k] = 0;
					s[t].x -= 147;
					animation = true;
				}
				else if (!tab[k - 1])
				{
					tab[k - 1] = tab[k];
					tab[k] = 0;
					s[t].x -= 147;
					animation = true;
				}
			}
			s[t].z = animationFrames;
		}
	}
	return deltaScore;
}

int Game::moveU()
{
	int deltaScore = 0;

	std::copy(tabDraw, tabDraw + 16, tab);

	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = 1; j < 4; j++)
		{
			unsigned k = j * 4 + i;
			if (!tab[k])
				continue;

			unsigned t = k;
			for (unsigned int l = 0; l < j; l++, k -= 4)
			{
				if ((tab[k] == tab[k - 4]) && tab[k] > 0)
				{
					deltaScore += pow(2, tab[k] + 1);
					tab[k - 4]++;
					tab[k - 4] *= -1;
					tab[k] = 0;
					s[t].y -= 147;
					animation = true;
				}
				else if (!tab[k - 4])
				{
					tab[k - 4] = tab[k];
					tab[k] = 0;
					s[t].y -= 147;
					animation = true;
				}
			}
			s[t].z = animationFrames;
		}
	}
	return deltaScore;
}

int Game::moveD()
{
	int deltaScore = 0;

	std::copy(tabDraw, tabDraw + 16, tab);

	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = 2; j < 10; j--)
		{
			unsigned k = j * 4 + i;
			if (!tab[k])
				continue;

			unsigned t = k;
			for (unsigned int l = j; l < 3; l++, k += 4)
			{
				if ((tab[k] == tab[k + 4]) && tab[k] > 0)
				{
					deltaScore += pow(2, tab[k] + 1);
					tab[k + 4]++;
					tab[k + 4] *= -1;
					tab[k] = 0;
					s[t].y += 147;
					animation = true;
				}
				else if (!tab[k + 4])
				{
					tab[k + 4] = tab[k];
					tab[k] = 0;
					s[t].y += 147;
					animation = true;
				}
			}
			s[t].z = animationFrames;
		}
	}
	return deltaScore;
}

void Game::update()
{
	for (int &i : tab)
	{
		if (i < 0)
			i *= -1;
	}

	bool upd = false;

	for (int i = 0; i < 16; i++)
	{
		if (tab[i] != tabDraw[i])
		{
			upd = true;
			break;
		}
	}

	std::copy(tab, tab + 16, tabDraw);

	if (upd)
		spawnTile();

	for (sf::Vector3f &i : s)
	{
		if (!i.z)
		{
			i.x = 0;
			i.y = 0;
		}
	}

	for (unsigned int y = 0, i = 0; y < 4; y++)
	{
		for (int x = 0; x < 4; x++, i += 4)
		{
			sf::Vector2f leUpCor(settings->margin * (x + 1) + 135 * x,
					settings->sepPosH + settings->margin * (y + 1) + y * 135);

			tiles[i].position = sf::Vector2f(leUpCor.x, leUpCor.y);
			tiles[i + 1].position = sf::Vector2f(leUpCor.x + settings->recSize - 1,
					leUpCor.y);
			tiles[i + 2].position = sf::Vector2f(leUpCor.x + settings->recSize - 1,
					leUpCor.y + settings->recSize - 1);
			tiles[i + 3].position = sf::Vector2f(leUpCor.x,
					leUpCor.y + settings->recSize - 1);

			unsigned int j = tabDraw[x + y * 4];

			tiles[i].texCoords = sf::Vector2f(j * 133, 0);
			tiles[i + 1].texCoords = sf::Vector2f(j * 133 + 133, 0);
			tiles[i + 2].texCoords = sf::Vector2f(j * 133 + 133, 133);
			tiles[i + 3].texCoords = sf::Vector2f(j * 133, 133);
		}
	}
}

bool Game::spawnTile()
{
	std::uniform_int_distribution<int> distribution(0, 15);

	bool found = false;

	for (int &i : tabDraw)
	{
		if (i == 0)
		{
			found = true;
			break;
		}
	}

	if (!found)
		return false;

	while (1)
	{
		int pos = distribution(generator);
		if (tabDraw[pos] == 0)
		{
			tabDraw[pos] = 1;
			return true;
		}
	}
}

void Game::animate(sf::Time frameTime)
{
	animationFrames = pow(frameTime.asSeconds(), -1) * settings->animationTime.asSeconds();

	if (animation)
	{
		frameTime = sf::seconds(
				frameTime.asSeconds() / settings->animationTime.asSeconds());

		for (int i = 0; i < 16; i++)
		{
			if (!s[i].z)
				continue;

			if (s[i].z == 1)
				animation = false;

			sf::Vector2f v(s[i].x * frameTime.asSeconds(),
					s[i].y * frameTime.asSeconds());
			s[i].z--;

			for (int k = i * 4; k < i * 4 + 4; k++)
			{
				tiles[k].position += v;
			}
		}

		if (!animation)
			update();
	}
}
