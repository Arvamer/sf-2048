//////////////////////////////////////////////////////////////////////////////////////
// The MIT License (MIT)
//
// Copyright (c) 2014 Mateusz Sieczko
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////////////

#ifndef TILE_H
#define TILE_H
#include "Config.hpp"
#include <SFML/Graphics.hpp>
#include <boost/filesystem.hpp>
#include <random>
#include <cmath>
#include <iostream>
#include <chrono>
#include <algorithm>
#include <fstream>
#include <cstdlib>
#include "Bg.hpp"
#include "Settings.hpp"


class Game: public sf::Drawable
{
public:
	Game(Bg *bg, const Settings *settings);
	int moveR();
	int moveL();
	int moveU();
	int moveD();
	void animate(sf::Time frameTime);
	void restart();
	void save();

	bool animation;
protected:

private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.texture = &texture;
		target.draw(tiles, states);
	}
	void update();
	bool spawnTile();

	Bg *bg;
	const Settings *settings;

	int animationFrames;

	boost::filesystem::path pa;

	int tabDraw[16];
	int tab[16];
	sf::Vector3f s[16];
	sf::VertexArray tiles;
	sf::Texture texture;
	std::default_random_engine generator;
};

#endif // TILE_H
