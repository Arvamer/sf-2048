//////////////////////////////////////////////////////////////////////////////////////
// The MIT License (MIT)
//
// Copyright (c) 2014 Mateusz Sieczko
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////////////

#include <SFML/Graphics.hpp>
#include <string>
#include "Config.hpp"
#include "Bg.hpp"
#include "Game.hpp"
#include "Settings.hpp"

using namespace sf;

int main()
{

	RenderWindow window(VideoMode(600, 650, 32), "sf::2048");
	window.setVerticalSyncEnabled(true);

	Settings settings;
	Bg bg(&settings);
	Game game(&bg, &settings);

	Clock clock;
	Time frameTime;

	while (window.isOpen())
	{
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
			{
				game.save();
				window.close();
			}

			if (Keyboard::isKeyPressed(Keyboard::Right) && !game.animation)
				bg.changeScore(game.moveR());
			if (Keyboard::isKeyPressed(Keyboard::Left) && !game.animation)
				bg.changeScore(game.moveL());
			if (Keyboard::isKeyPressed(Keyboard::Down) && !game.animation)
				bg.changeScore(game.moveD());
			if (Keyboard::isKeyPressed(Keyboard::Up) && !game.animation)
				bg.changeScore(game.moveU());
			if (Mouse::isButtonPressed(Mouse::Left))
			{
				Vector2i pos = Mouse::getPosition(window);
				if (pos.x >= 550 && pos.x <= 582 && pos.y >= 8 && pos.y <= 40)
				{
					game.restart();
					bg.restart();
				}
			}
		}

		game.animate(frameTime);

		window.clear(Color(127, 127, 127));

		bg.draw(&window);
		window.draw(game);

		window.display();

		frameTime = clock.restart();
	}
	return 0;
}
